const add = (string, structure = [';', '/', '\n']) => {
  if (!string) {
    return 0
  }
  let str = string;

  for (let i = 0, len = structure.length; i < len; i++) {
    str = str.split(structure[i]).join(',')
  }

  return str
    .split(',')
    .filter(item => item !== '')
    .map(item => parseInt(item, 10))
    .reduce((prev, curr) => prev + curr);
};

console.log(add('//;\n1;22'));
