function add(string) {
  const validate = /\S/g.test(string);

  if (!string || !validate)
    return 0;

  return string.split(',')
    .reduce((prev, curr) => prev + parseInt(curr), 0);
}

console.log(add('1,2,322'));
