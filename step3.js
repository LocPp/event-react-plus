function add(string) {
  const validate = /\S/g.test(string);

  if (!string || !validate)
    return 0;

  const regex = /\d+/g;

  return string
    .match(regex)
    .reduce((prev, curr) => prev + parseInt(curr), 0)
}

console.log(add('1\n2\n3'));
